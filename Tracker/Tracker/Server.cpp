#include "Server.h"
using namespace requests;
using namespace FailureReason;

Server* Server::_Instance = nullptr;

/// <summary>
/// Constructor.
/// </summary>
Server::Server()
{
	this->_torrentManager = TorrentManager::getInstance(_torrents);
	_torrentManager->loadTorrents();
	std::cout << "Tracker is available :)\n";
}

/// <summary>
/// getter for Server instance.
/// </summary>
/// <returns>Server instance.</returns>
Server* Server::getInstance() noexcept
{
	if (_Instance == nullptr) {
		_Instance = new Server();
	}
	return _Instance;
}

/// <summary>
/// This function starts listening for requests.
/// </summary>
void Server::run()
{
	this->_svr.Get("/announce", [&](const httplib::Request& r, httplib::Response& res) {
		auto c = generateResponse(r, res);
		res.set_content(c, "text/plain");
		
		});

	_svr.set_keep_alive_timeout(120);
	this->_svr.listen("0.0.0.0", this->_port);
}

/// <summary>
/// This function generates a response to the request.
/// </summary>
/// <param name="r"> the request.</param>
/// <param name="res"> referrence to the response.</param>
/// <returns> the content of the response.</returns>
const std::string Server::generateResponse(const httplib::Request& r, httplib::Response& res)
{
	std::cout << std::format("Request from id {}\n", r.get_param_value("peerID"));
	std::tuple<std::shared_ptr<PeerRequest>, ResponseCodes> resTuple = isRequestValid(r);
	std::shared_ptr<PeerRequest> req = std::get<0>(resTuple);
	PeerResponse resStruct;
	res.status = (unsigned int)std::get<1>(resTuple);
	std::string toReturn = "";
	if (req.get() == nullptr)
	{
		auto reason = failureReasons.find((ResponseCodes)res.status);
		resStruct.failureReason = reason->second;
		return bencode::encode(bencode::dict{ { "failure reason",resStruct.failureReason} });
	}
	if (_torrents.contains(req.get()->infoHash))
	{
		if (req->event == EventFieldOptions::Stopped)
		{
			//This peer stoped the downloading process, removing it from the list.
			_torrents[req->infoHash].erase(req->peerID);
			std::cout << "event\n";
			return toReturn; //return empty response.
		}
	}
	else //new torrent
	{
		if (_torrents.size() == TrackersNumwant) //check if tracker reached max capacity.
		{
			// remove a random torrent
			_torrents.erase(_torrents.begin());
		}
		_torrents.insert(std::make_pair(req->infoHash, PeerList{}));
	} 
	
	//add torrent and/or peer.
	auto it = _torrents.find(req->infoHash);
	PeerList peers = it->second;
	peers[req->peerID] = Peer{ req->peerID, req->ip, req->port };
	_torrents[req->infoHash] = peers;
	_torrentManager->saveTorrents();


	peers.erase(req->peerID);
	resStruct.peers = peers;
	resStruct.interval = AnnounceInterval;
	resStruct.minInterval = AnnounceMinInterval;
	//TO DO: support complete, incomplete and trackerID.

	std::cout << "Response:\n";
	for (auto it = peers.cbegin(); it != peers.cend(); ++it)
	{
		std::cout << it->first << "\n";
	}

	return toDictionaryResponse(resStruct, req->noPeerID);
}

/// <summary>
/// This function checks if request is valid.
/// </summary>
/// <param name="r"> the request.</param>
/// <returns> tuple of PeerRequest and response code.</returns>
const std::tuple<std::shared_ptr<PeerRequest>, ResponseCodes> Server::isRequestValid(const httplib::Request& r) const noexcept
{
	PeerRequest req;
	if(!r.has_param("infoHash"))
	{
		return std::make_tuple(std::shared_ptr<PeerRequest>(nullptr), ResponseCodes::MissingInfoHash);
	}
	if (!r.has_param("peerID"))
	{
		return std::make_tuple(std::shared_ptr<PeerRequest>(nullptr), ResponseCodes::MissingPeerID);
	}
	if (!r.has_param("port"))
	{
		return std::make_tuple(std::shared_ptr<PeerRequest>(nullptr), ResponseCodes::MissingPort);
	}
	for (auto& param : r.params)
	{
		auto validator = requestToValidator.find(param.first); //find validator
		if (validator == requestToValidator.end())
		{
			return std::make_tuple(std::shared_ptr<PeerRequest>(nullptr), ResponseCodes::GenericError);
		}
		if (!validator->second(param.second))
		{
			auto resCode = requestToFaliureReason.find(param.first);
			return std::make_tuple(std::shared_ptr<PeerRequest>(nullptr), resCode->second);
		}
	}
	req.infoHash = httplib::detail::decode_url(r.get_param_value("infoHash"), true);
	req.peerID = httplib::detail::decode_url(r.get_param_value("peerID"), true);
	req.port = std::stoul(r.get_param_value("port"), 0, 10);
	if (r.has_param("uploaded")) { //optional
		req.uploaded = std::stoul(r.get_param_value("uploaded"), 0, 10);
	}
	if (r.has_param("downloaded")) { //optional
		req.downloaded = std::stoul(r.get_param_value("downloaded"), 0, 10);
	}
	if (r.has_param("left")) { //optional
		req.left = std::stoul(r.get_param_value("left"), 0, 10);
	}
	if (r.has_param("compact")) { //only 0.
		std::istringstream(r.get_param_value("compact")) >> req.compact; //from string to bool.
	}
	if (r.has_param("noPeerID")) {
		std::istringstream(r.get_param_value("noPeerID")) >> req.noPeerID; //from string to bool.
	}
	if (r.has_param("event")) {
		req.event = (EventFieldOptions)std::stoul(r.get_param_value("event"), 0, 10);
	}
	if (r.has_param("ip")) { //optional
		req.ip = r.get_param_value("ip");
	}
	if (r.has_param("numwant")) { //optional
		req.numwant = std::stoul(r.get_param_value("numwant"), 0, 10);
	}
	if (r.has_param("key")) { //optional
		req.key = r.get_param_value("key");
	}
	if (r.has_param("trackerID")) { //optional
		req.trackerID = r.get_param_value("trackerID");
	}
	return std::make_tuple(std::make_shared<PeerRequest>(req), ResponseCodes::Ok);
}

/// <summary>
/// This function builds the dictionary response(bencode dict) from the response struct.
/// </summary>
/// <param name="resStruct"> struct that contains all the response fields.</param>
/// <param name="noPeerID"> if true peerID field wont be included, false- otherwise.</param>
/// <returns>string that represents a bencoded dict.</returns>
std::string Server::toDictionaryResponse(PeerResponse& resStruct, bool noPeerID) const noexcept
{
	bencode::dict replyDict;
	bencode::list peerList;
	replyDict["interval"] = resStruct.interval;
	replyDict["min interval"] = resStruct.minInterval;
	
	//TO DO: support complete, incomplete and trackerID.
	
	//replyDict["tracker id"] = resStruct.trackerID;
	//replyDict["complete"] = resStruct.complete;
	//replyDict["incomplete"] = resStruct.incomplete;
	
	for (auto& peer : resStruct.peers)
	{
		peerList.push_back(peer.second.toEntry(noPeerID));
	}
	replyDict["peers"] = peerList;
	return bencode::encode(replyDict);
}