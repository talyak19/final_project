#include "Server.h"
#include <iostream>

int main(void)
{
	Server* s = Server::getInstance();
	s->run();
	return 0;
}