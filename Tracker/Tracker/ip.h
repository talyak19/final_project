#pragma once
#include <regex>

namespace ip
{
	const std::regex IPV4("(([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])");
	const std::regex IPV6("((([0-9a-fA-F]){1,4})\\:){7}([0-9a-fA-F]){1,4}");
}