#include "TorrentManager.h"
#include <fstream>

TorrentManager* TorrentManager::_Instance = nullptr;

/// <summary>
/// Constructor.
/// </summary>
/// <param name="torrents"> ref to torrent list</param>
TorrentManager::TorrentManager(TorrentList& torrents) : _torrents(torrents)
{
}

/// <summary>
/// getter for TorrentManager Instance.
/// </summary>
/// <param name="torrents"> ref to torrent list.</param>
/// <returns>TorrentManager instance.</returns>
TorrentManager* TorrentManager::getInstance(TorrentList& torrents) noexcept
{
	if (_Instance == nullptr) {
		_Instance = new TorrentManager(torrents);
	}
	return _Instance;
}

/// <summary>
/// This function saves the info hashes in the file.
/// </summary>
/// <returns> none</returns>
void TorrentManager::saveTorrents() const noexcept
{
	std::ofstream file(PATH, std::ofstream::trunc);
	for (auto &torrent : this->_torrents)
	{
		file << torrent.first << "\n";
	}
}

/// <summary>
/// This function loads the info hashes from the file.
/// </summary>
/// <returns> none.</returns>
void TorrentManager::loadTorrents() const noexcept
{
	std::ifstream file(PATH);
	for (std::string line; getline(file, line); )
	{
		_torrents[line] = PeerList();
	}
}