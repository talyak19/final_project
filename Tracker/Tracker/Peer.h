#pragma once
#include "bencode.hpp"

typedef struct Peer
{
	std::string peerID{};
	std::string ip{};
	unsigned int port{};
	std::map<std::string, bencode::data> toEntry(bool noPeerID);
}Peer;

