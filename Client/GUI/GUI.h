#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_GUI.h"
#include <vector>

#include "Client.h"
#include <chrono>
#include <thread>
#include <fstream>
#include <bitset>
#include <sys/stat.h>
#include "bencode.hpp"
#include <filesystem>
#include <unordered_set>
#include "qmainwidget.h"

class QMainWidget;


class GUI : public QMainWindow
{
    Q_OBJECT

public:
    GUI(Client& client, QWidget* parent = Q_NULLPTR);
    ~GUI();

public slots:
    void on_actionAdd_torrent_triggered();
    void on_actionGenerate_torrent_triggered();

private:
    Ui::GUIClass _ui;
    Client& _client;
    QMainWidget* _torrentsList;
};
