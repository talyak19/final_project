#include "thewidgetitem.h"
#include "ui_thewidgetitem.h"

TheWidgetItem::TheWidgetItem(QString label, const TorrentHandler* handler, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TheWidgetItem), _handler(handler), _progress(0)
{
    ui->setupUi(this);
    ui->label->setText(label);
    handler->onPieceDownload(std::bind(&TheWidgetItem::sendDownloadProgress, this));
    sendDownloadProgress();
}

TheWidgetItem::~TheWidgetItem()
{
    delete ui;
}

void TheWidgetItem::sendDownloadProgress()
{
    QMetaObject::invokeMethod(this, // obj
        "updateProgress", // member
        Qt::QueuedConnection, // connection type
        Q_ARG(int, _handler->getPercentDownloaded())); // val1
}

void TheWidgetItem::updateProgress(int progress)
{
    _progress = progress;
    ui->progressBar->setValue(_progress);
}
