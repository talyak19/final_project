#pragma once

#undef UNICODE

#define WIN32_LEAN_AND_MEAN
#define FD_SETSIZE 300

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <iostream>
#include <exception>
#include "TorrentHandler.h"
#include "Communication.h"
#include <vector>
#include <mutex>
#include "thread_pool.hpp"
#include "cpp-httplib-master/httplib.h"
// Need to link with Ws2_32.lib
#pragma comment (lib, "Ws2_32.lib")
// #pragma comment (lib, "Mswsock.lib")

#define DEFAULT_BUFLEN 512

struct PeerInfoResponse
{
	std::string id;
	std::string ip;
	std::string port;
};

struct ConnectedPeer
{
	SOCKET sock;
	PeerInfoResponse info;
};

class Client
{
public:
	//Temporary string to be used for testing
	//SHOULD BE REMOVED FOR RELEASE
	std::string _takenPort;

	Client();
	void startConnection();
	const TorrentHandler* addTorrent(const std::string & torrentPath, const std::string & dstPath);
	const TorrentHandler* addTorrent(const std::string& torrentPath);


	~Client();
private:
	static constexpr char _InitialPort[] = DEFAULT_PORT;
	static constexpr char _MaxPort[] = "27115";

	static constexpr int _IdLength = 20;
	static constexpr std::string_view _ClientVersion = "1000";
	std::string _id;

	thread_pool _pool;
	std::thread _peerFinderThread;
	std::thread _messengerThread;

	SOCKET _listenSocket;
	std::mutex _startupMtx;
	std::condition_variable _startupCv;

	std::shared_mutex _torrentsMtx;
	std::condition_variable_any _torrentsCv;

	std::unordered_map<std::string, std::unique_ptr<TorrentHandler>> _activeTorrents;

	void listen(std::string port);
	void addPeer(SOCKET peerSock);
	void acceptClients();

	std::vector<PeerInfoResponse>  requestPeersFromServer(std::string_view infoHash);
	static ConnectedPeer connectToPeer(const PeerInfoResponse& peer);
	void getPeers(std::string_view infoHash);
	void continuouslyFindPeers();
	
	bool areTorrentsMatching(const std::vector< TorrentHandler*>& handlers);
	void messageWithHandlers();

	static constexpr std::string_view _TrackersIP = "127.0.0.1";
	static constexpr unsigned int _TrackersPort = 17670;

	httplib::Client _httpClient;

};

