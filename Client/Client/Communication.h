#pragma once

#undef UNICODE

#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <iostream>
#include "Messages.h"

// Need to link with Ws2_32.lib
#pragma comment (lib, "Ws2_32.lib")
// #pragma comment (lib, "Mswsock.lib")

using namespace Messages;

namespace Communication
{
	static constexpr int _LengthFieldSize = 4;


	int sendToSocket(SOCKET peer, const std::vector<unsigned char>& msg);
	std::shared_ptr<char> recvFromSocket(SOCKET sock, int bytesAmount);

	void sendHandshake(SOCKET peer, std::string_view hash, std::string_view id, std::string_view ip = "", std::string_view port = "");

	HandshakeMessage receiveHandshake(SOCKET sock);

	
	struct PollResults
	{
		std::vector<SOCKET> badSockets;
		std::vector<SOCKET> eventFulfillingSockets;
	};

	std::shared_ptr<PollResults> pollSockets(const std::vector<SOCKET>& socketsToCheck, int events);
	std::shared_ptr<PollResults> pollSockets(const std::vector<SOCKET>& socketsToCheck, int events, int waitTime);
	std::shared_ptr<PollResults> getReadableSockets(const std::vector<SOCKET>& socketsToCheck);
	std::shared_ptr<PollResults> getReadableSockets(const std::vector<SOCKET>& socketsToCheck, int waitTime);
	std::shared_ptr<PollResults> getWriteableSockets(const std::vector<SOCKET>& socketsToCheck);
	std::shared_ptr<PollResults> getWriteableSockets(const std::vector<SOCKET>& socketsToCheck, int waitTime);

}
