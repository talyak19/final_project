#pragma once
#include <memory>
#include <vector>

namespace Bitsets
{
	using DynamicBitset = std::vector<bool>;
	using StaticBitset = std::unique_ptr<bool[]>;
	static constexpr auto& createStaticBitset = std::make_unique<bool[]>;

	constexpr int _ByteSize = 8;

	std::vector<unsigned char> convertFromBitfield(DynamicBitset bitfield) noexcept;
}