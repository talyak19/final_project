#pragma once

#undef UNICODE

#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <format>
#include <iostream>
#include <string>
#include <string_view>
#include <exception>
#include <vector>
#include <thread>
#include <mutex>
#include <map>
#include <queue>
#include <fstream>
#include "Exceptions.h"
#include "Messages.h"
#include "Torrents.h"
#include "SingleFileManager.h"
#include "MultipleFileManager.h"
#include "Communication.h"
#include "Bitsets.h"
#include "Peer.h"
// Need to link with Ws2_32.lib
#pragma comment (lib, "Ws2_32.lib")
// #pragma comment (lib, "Mswsock.lib")

#define DEFAULT_PORT "27015"

using namespace Messages;
using namespace Torrents;
using namespace Bitsets;
using namespace Communication;

constexpr int ipow(int base, int exp, int result = 1)
{
	return exp < 1 ? result : ipow(base * base, exp / 2, (exp % 2) ? result * base : result);
}

class TorrentHandler
{
	using enum MessageType;
public:
	TorrentHandler(const Torrent& t, const std::string& path, const std::string& infoHash);
	~TorrentHandler();

	const Torrent& getTorrent() const noexcept;
	
	void addPeer(SOCKET incomingPeer, std::string_view id);

	static bool isPeerListFull() noexcept;
	bool isPeerInList(std::string_view peerId) const noexcept;

	void messagePeers();
	void onPieceDownload(std::function<void()> function) const;
	float getPercentDownloaded() const;
private:

	static constexpr int _MaxSubPieceRequests = 5;
	static constexpr int _PieceBlockSize = ipow(2, 14);

	static constexpr int _MaxPeers = 400;
	static constexpr int _MaxPendingRequests = 5;
	static int _PeerCount;

	static std::unordered_map<std::string, TorrentHandler> _InfoHashToHandler;

	const Torrent _targetTorrent;
	const std::string _infoHash;

	FileManager* _fileManager;
	bool _isSeeding;

	std::thread _messengerThread;

	std::vector<std::unique_ptr<Peer>> _peers;

	std::vector<std::unique_ptr<Peer>> _peerQueue;
	std::mutex _peerQueueMtx;
	std::condition_variable _peerQueueCv;

	mutable std::shared_mutex _idsMutex;
	std::vector<std::string> _knownIds;

	int _bytesDownloaded;
	mutable std::vector<std::function<void()>> onPieceDownloadList;

	//<piece index> to <block bitset>
	std::map<int, StaticBitset> _blocksRequested;
	//<piece index> to <block bitset>
	std::map<int, StaticBitset> _blocksDownloaded;

	bool isPeerInList(std::string_view peerId, bool lockMtx) const noexcept;
	void insertIncomingPeers() noexcept;

	int getBlocksAmountInPiece(int pieceIndex) const noexcept;
	void requestPieceBlock(Peer* peer, int pieceIndex);
	void saveBlockInFile(int index, int begin, const std::vector<unsigned char>& block);

	int respondToPieceRequest(Peer& peer, const RequestMessage& req);
	int findOptimalPiece(Peer* peer);

	void handlePollResult(const PollResults& res, std::vector<Peer*>& resVec);
	void handleIncomingMessages();
	void handleOutcomingMessages();
};

