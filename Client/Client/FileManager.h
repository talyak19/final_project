#pragma once
#include <fstream>
#include <iostream>
#include <vector>
#include <filesystem>
#include <algorithm>
#include <ranges>
#include "Torrents.h"

using namespace Torrents;

class FileManager
{
public:

	FileManager(std::string_view path, const TorrentInfo* info);

	const std::vector<bool>& getBitfield() const noexcept;

	virtual void write(int position, const std::vector<unsigned char>& content) = 0;
	virtual std::vector<unsigned char> read(int position, int length) = 0;
	virtual void updateBitfield() = 0;
	virtual void updateBitfield(int pieceIndex);

	virtual bool finishedDownloading() const noexcept;


private:
	const TorrentInfo* _rawInfo;

protected:
	std::string _targetPath;
	std::vector<bool> _piecesBitfield;
};

