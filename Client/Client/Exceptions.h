#pragma once
#include <iostream>
#include <winsock2.h>

class EstablishConnectionError :
    public std::exception
{
public:
    EstablishConnectionError(std::string_view ip, std::string_view port);
    virtual char const* what() const noexcept override;
    std::string_view getIp() const noexcept;
    std::string_view getPort() const noexcept;

private:
    std::string _ip;
    std::string _port;

};

class PeerConnectionError :
    public std::exception
{
public:
    PeerConnectionError(SOCKET socket) noexcept;
    PeerConnectionError(SOCKET sock, std::string_view id) noexcept;
    virtual char const* what() const noexcept override;

    int getSocket() const noexcept;
    std::string_view getId();

private:
    SOCKET _sock;
    std::string _id;
    std::string _ip;
    std::string _port;
};

class MaxPeersReached :
    public  std::exception
{
public:
    virtual char const* what() const noexcept override;
};


class PeerAlreadyKnown :
    public PeerConnectionError
{
public:
    using PeerConnectionError::PeerConnectionError;

    virtual char const* what() const noexcept override;

};

class UnknownPeer :
    public PeerConnectionError
{
public:
    using PeerConnectionError::PeerConnectionError;
    virtual char const* what() const noexcept override;
};
