#pragma once
#include <vector>
#include "serializer-master/serializer.h"
#include <tuple>
#include "Torrents.h"

namespace Messages
{
	static constexpr int lengthSize = 4;
	enum class MessageType : unsigned char
	{
		Handshake, Have = 20, Bitfield, Request, Piece, Cancel
	};

	struct HandshakeMessage
	{
		static constexpr MessageType id = MessageType::Handshake;

		static constexpr std::string_view pstr = "syncpp";
		static constexpr int pstrlen = pstr.size();
		std::string peerId;
		std::string infoHash;

		static constexpr int length = pstrlen + sizeof(pstrlen) + 
			Torrents::hashLen + 20 + lengthSize * 3;
	};

	template <typename Archive>
	void serialize(Archive& archive, HandshakeMessage& msg)
	{
		archive((int)msg.pstrlen, std::string(msg.pstr.data()),
			msg.infoHash, msg.peerId);
	}

	struct HaveMessage
	{
		static constexpr MessageType id = MessageType::Have;
		int index;
	};

	template <typename Archive>
	void serialize(Archive& archive, HaveMessage& msg)
	{
		archive(msg.index);
	}


	struct BitfieldMessage
	{
		static constexpr MessageType id = MessageType::Bitfield;
		std::vector<unsigned char> bitfield;
	};

	template <typename Archive>
	void serialize(Archive& archive, BitfieldMessage& msg)
	{
		archive(msg.bitfield);
	}


	struct RequestMessage
	{
		static constexpr MessageType id = MessageType::Request;
		int index;
		int begin;
		int length;
	};

	template <typename Archive>
	void serialize(Archive& archive, RequestMessage& msg)
	{
		archive(msg.index, msg.begin, msg.length);
	}


	struct PieceMessage
	{
		static constexpr MessageType id = MessageType::Piece;
		int index;
		int begin;
		std::vector<unsigned char> block;
	
	};

	template <typename Archive>
	void serialize(Archive& archive, PieceMessage& msg)
	{
		archive(msg.index, msg.begin, msg.block);
	}


	struct CancelMessage
	{
		static constexpr MessageType id = MessageType::Cancel;
		int index;
		int begin;
		int length;
	};

	template <typename Archive>
	void serialize(Archive& archive, CancelMessage& msg)
	{
		archive(msg.index, msg.begin, msg.length);
	}

	template<typename Msg>
	concept Message = requires(Msg msg)
	{
		{msg.id};
	};


	template <Message Msg>
	class MessageSerializer
	{
	public:
		MessageSerializer(Msg& message);
		std::vector<unsigned char> data;
	};

	template <Message Msg>
	class MessageDeserializer
	{
	public:
		MessageDeserializer(std::vector<unsigned char>& data);

		Msg message;
	};

	template<Message Msg>
	inline MessageSerializer<Msg>::MessageSerializer(Msg& message)
	{
		zpp::serializer::memory_output_archive out(data);
		out(message);
	}


	template<Message Msg>
	inline MessageDeserializer<Msg>::MessageDeserializer(std::vector<unsigned char>& data)
	{
		zpp::serializer::memory_input_archive in(data);
		in(message);
	}

}

