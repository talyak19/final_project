#include "Client.h"
#include <random>

#include "bencode.hpp"
#include <unordered_set>
#include <set>
#include <cassert>

Client::Client() : _listenSocket(INVALID_SOCKET), _httpClient(_TrackersIP.data(), _TrackersPort)
{
	int iResult = 0;
	WSADATA wsaData;

	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		printf("WSAStartup failed with error: %d\n", iResult);
		throw std::exception();
	}

	_id = _ClientVersion.data();

	_httpClient.set_read_timeout(120, 0);

	//Generate random string
	//std::random_device rd;
	//std::mt19937 mt(rd());
	//auto randchar = [&mt]() -> char
	//{
	//	const char charset[] =
	//		"0123456789"
	//		"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	//		"abcdefghijklmnopqrstuvwxyz";
	//	const size_t max_index = (sizeof(charset) - 1);
	//	std::uniform_int_distribution<int> dist(0, max_index);

	//	return charset[dist(mt)];
	//};
	/*_id.resize(_IdLength);
	std::generate_n(_id.begin() + _ClientVersion.size(),
		_IdLength - _ClientVersion.size(), randchar);*/


	int remLen = _IdLength - _ClientVersion.size();
	static auto& chrs = "0123456789"
		"abcdefghijklmnopqrstuvwxyz"
		"ABCDEFGHIJKLMNOPQRSTUVWXYZ";

	thread_local static std::mt19937 rg{ std::random_device{}() };
	thread_local static std::uniform_int_distribution<std::string::size_type> pick(0, sizeof(chrs) - 2);

	std::string s;

	s.reserve(remLen);

	while (remLen--)
		s += chrs[pick(rg)];

	_id = _id + s;
	std::cout << std::format("Generated id {}\n", _id);

}

Client::~Client()
{
	closesocket(_listenSocket);
	std::cout << "Closing...\n";
	WSACleanup();
	exit(1);
}

/// <summary>
/// Sets up all necessary functions and starts accepting clients
/// </summary>
void Client::startConnection()
{
	std::unique_lock lck(_startupMtx);

	//Try to bind a port by trying ports with an ascending number
	std::string currentPort = _InitialPort;
	while (_takenPort != currentPort)
	{
		try
		{
			listen(currentPort);
			_takenPort = currentPort;
		}
		catch (const std::exception&)
		{
			currentPort = std::to_string(std::stoi(currentPort) + 1);
		}
	}
	std::cout << std::format("Listening on port {}\n", _takenPort);

	//Allow addition of torrents
	lck.unlock();
	_startupCv.notify_all();

	_peerFinderThread = std::thread(&Client::continuouslyFindPeers, this);
	_peerFinderThread.detach();
	_messengerThread = std::thread(&Client::messageWithHandlers, this);
	_messengerThread.detach();

	try
	{
		acceptClients();
	}
	catch (const std::exception&)
	{
		std::cout << "Error while accepting client\n";
		exit(1);
	}


}

/// <summary>
/// Sends a request to a tracker server to get peers' info
/// </summary>
/// <returns>vector of ips and ports of peers</returns>
std::vector<PeerInfoResponse> Client::requestPeersFromServer(std::string_view infoHash)
{
	std::vector<PeerInfoResponse> peersInfo;

	//httplib::Client cli(_TrackersIP.data(), _TrackersPort);
	httplib::Params myParams;

	myParams.insert(std::make_pair("infoHash", httplib::detail::encode_url(infoHash.data())));
	myParams.insert(std::make_pair("peerID", httplib::detail::encode_url(_id)));
	myParams.insert(std::make_pair("port", _takenPort));
	myParams.insert(std::make_pair("compact", "0"));
	myParams.insert(std::make_pair("noPeerID", "0"));
	myParams.insert(std::make_pair("ip", "127.0.0.1"));

	auto res = _httpClient.Get("/announce", myParams, httplib::Headers());
	if (res->status != 200)
	{
		return peersInfo;
	}
	try
	{
		std::string resBody = res->body;
		auto data = bencode::decode(resBody);
		auto& dict = boost::get<bencode::dict>(data);
		auto& peers = boost::get<bencode::list>(dict["peers"]);
		for (auto& peer : peers)
		{
			auto& peerDict = boost::get<bencode::dict>(peer);
			peersInfo.push_back(PeerInfoResponse{
				boost::get<bencode::string>(peerDict["id"]),
				boost::get<bencode::string>(peerDict["ip"]),
				std::to_string(boost::get<bencode::integer>(peerDict["port"])) });
		}
	}
	catch (...)
	{
		std::cout << "err while request from server\n";
	}

	return peersInfo;

}

/// <summary>
/// Connects to all given peers
/// </summary>
/// <param name="peers">Target peers</param>
/// <returns>Vector of successfuly connected peers</returns>
ConnectedPeer Client::connectToPeer(const PeerInfoResponse& peerInfo)
{
	std::vector<ConnectedPeer> connectedPeers;

	SOCKET connectSocket = INVALID_SOCKET;
	struct addrinfo* result = NULL,
		* ptr = NULL,
		hints;
	int iResult;

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	// Resolve the peer address and port
	iResult = getaddrinfo(peerInfo.ip.c_str(), peerInfo.port.c_str(),
		&hints, &result);
	if (iResult != 0)
	{
		printf("getaddrinfo failed with error: %d\n", iResult);
		throw std::exception();
	}

	// Attempt to connect to an address until one succeeds
	for (ptr = result; ptr != NULL; ptr = ptr->ai_next)
	{
		// Create a SOCKET for connecting to peer
		connectSocket = socket(ptr->ai_family, ptr->ai_socktype,
			ptr->ai_protocol);
		if (connectSocket == INVALID_SOCKET)
		{
			printf("socket failed with error: %ld\n", WSAGetLastError());
			throw std::exception();
		}

		// Connect to peer.
		iResult = connect(connectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
		if (iResult == SOCKET_ERROR)
		{
			closesocket(connectSocket);
			connectSocket = INVALID_SOCKET;
			continue;
		}
		break;
	}

	freeaddrinfo(result);

	if (connectSocket == INVALID_SOCKET)
	{

		throw EstablishConnectionError(peerInfo.ip, peerInfo.port);
	}

	return ConnectedPeer{ connectSocket, peerInfo };
}

/// <summary>
/// Requests peers from server for torrent with given info hash and 
/// tries to connect to all of them using thread pool
/// </summary>
/// <param name="infoHash">The hash of the torrent to get peers for</param>
void Client::getPeers(std::string_view infoHash)
{
	if (TorrentHandler::isPeerListFull())
	{
		return;
	}

	auto peerResponse = requestPeersFromServer(infoHash);
	if (peerResponse.empty())
	{
		return;
	}

	auto it = peerResponse.begin();
	while (it != peerResponse.end())
	{
		PeerInfoResponse info = *it;
		//Get rid of peers with an already known id while also
		//Making sure a peer with the same address or id as this client isn't added
		if (_activeTorrents[infoHash.data()]->isPeerInList(info.id) ||
			(info.ip == "127.0.0.1" && info.port == _takenPort) ||
			info.id == _id)
		{
			it = peerResponse.erase(it);
		}
		else 
		{
			++it;
		}
	}

	std::vector<std::future<ConnectedPeer>> peersFutures;
	//Use thread pools to connect to all peers quickly
	for (auto& peer : peerResponse)
	{
		auto future = _pool.submit([=]() { return connectToPeer(peer); });
		peersFutures.push_back(std::move(future));
	}

	std::vector<SOCKET> socks;
	//Check all of the peers and make sure the connection succeeded
	for (auto& future : peersFutures)
	{
		ConnectedPeer peer{};

		try
		{
			peer = future.get();
			Communication::sendHandshake(peer.sock, infoHash, _id,
				peer.info.ip, peer.info.port);
			socks.push_back(peer.sock);
		}
		catch (EstablishConnectionError& e)
		{
			std::cout << std::format(
				"Unable to connect to peer with ip {} and port {}\n",
				e.getIp(), e.getPort());
		}


	}

	auto res = getReadableSockets(socks, -1);
	if (!res)
	{
		return;
	}

	auto& goodSocks = res->eventFulfillingSockets;
	auto& badSocks = res->badSockets;

	//Receive handshake and start normal communication
	for (int i = 0; i < goodSocks.size(); i++)
	{
		HandshakeMessage msg = Communication::receiveHandshake(goodSocks[i]);

		if (!_activeTorrents.count(msg.infoHash))
		{
			std::cout << std::format(
				"An incorrect info hash was returned from peer {}\n",
				goodSocks[i]);
			continue;
		}

		
		_activeTorrents[msg.infoHash]->addPeer(goodSocks[i], msg.peerId);
	}

	for (int i = 0; i < badSocks.size(); i++)
	{
		std::cout << std::format("Closed connection with sock {}", badSocks[i]);
		closesocket(badSocks[i]);
	}

}

/// <summary>
/// Goes through all active torrents and searches peers for each of them
/// </summary>
void Client::continuouslyFindPeers()
{
	const int waitTime = 2;
	
	while (true)
	{
		std::shared_lock lck(_torrentsMtx);

		//Wait for duration or until a new torrent is added
		auto waitingStartTime = std::chrono::high_resolution_clock::now();
		while (_torrentsCv.wait_for(lck, std::chrono::seconds(waitTime),
			[&waitingStartTime]{return  duration_cast<std::chrono::seconds>(
				std::chrono::high_resolution_clock::now() - 
				waitingStartTime).count() >= waitTime; }) ==
			false)
		{
		}

		for (auto& handler : _activeTorrents)
		{
			_pool.push_task([&]() { getPeers(handler.first); });
		}

	}
}

bool Client::areTorrentsMatching(const std::vector<TorrentHandler*>& handlers)
{
	if (_activeTorrents.size() != handlers.size())
	{
		return false;
	}

	//Check if there is a handler for a torrent that is not in 
	//the argument handler list
	for (auto& torrent : _activeTorrents)
	{
		//Check if torrent is in argument handler list
		bool found = std::ranges::count_if(handlers, [&](
			const TorrentHandler* p) { return p == torrent.second.get(); });

		if (!found)
		{
			return false;
		}
	}

	return true;
}

/// <summary>
/// Goes through all handlers and call their message function
/// </summary>
void Client::messageWithHandlers()
{
	std::vector<TorrentHandler*> handlers;
	std::unordered_set<TorrentHandler*> waitingHandlers;
	std::unordered_set<TorrentHandler*> activeHandlers;
	std::mutex handlersMtx;
	std::condition_variable handlersCv;

	int addedCount = 0;
	auto manageHandlers = [&]() {
		while (true)
		{
			std::shared_lock lck(_torrentsMtx);
			if (_activeTorrents.empty())
			{
				//Wait until there is an active torrent
				_torrentsCv.wait(lck, [this] {
					return !_activeTorrents.empty(); });
			}
			else
			{
				//Wait until torrent list is changed
				_torrentsCv.wait(lck, [&] {
					return !areTorrentsMatching(handlers); });
			}

			//Add all torrents to temporary vector
			std::vector<TorrentHandler*> newHandlers;
			for (auto& torrent : _activeTorrents)
			{
				newHandlers.push_back(torrent.second.get());
			}
			lck.unlock();

			for (auto& handler : handlers)
			{
				//If handler is not found in new list; If deleted from
				//list by user
				if (std::find(newHandlers.begin(), newHandlers.end(), handler)
					== newHandlers.end())
				{
					//!!!
					//Shouldn't enter here as deleting isn't supported yet
					//!!!
					assert(false);

					//Remove deleted handler from sets
					std::scoped_lock handlersLck(handlersMtx);
					if (waitingHandlers.contains(handler))
					{
						waitingHandlers.erase(handler);
					}
					else if (activeHandlers.contains(handler))
					{
						activeHandlers.erase(handler);
					}
				}
			}

			handlers = newHandlers;

			//Insert all new handlers to waiting set
			std::scoped_lock handlersLck(handlersMtx);
			for (auto& handler : newHandlers)
			{
				if (!waitingHandlers.contains(handler) &&
					!activeHandlers.contains(handler))
				{
					waitingHandlers.insert(handler);
					addedCount++;
				}
			}
			handlersCv.notify_all();
		}
	};

	std::thread handlerManagerThread(manageHandlers);
	handlerManagerThread.detach();

	auto callHandlerMessenger = [&waitingHandlers, &activeHandlers,
		&handlersMtx, &handlersCv](TorrentHandler* handler, int cycles)
	{
		for (int i = 0; i < cycles; i++)
		{
			handler->messagePeers();
		}

		std::scoped_lock handlersLck(handlersMtx);
		waitingHandlers.insert(handler);
		activeHandlers.erase(handler);
		handlersCv.notify_all();

	};

	while (true)
	{
		std::vector<TorrentHandler*> queuedHandlers;
		std::unordered_set<TorrentHandler*> waitingHandlersCopy;

		std::unique_lock lck(handlersMtx);
		if (waitingHandlers.empty())
		{
			handlersCv.wait(lck, [&waitingHandlers] {
				return !waitingHandlers.empty(); });
		}

		//Create copy of waiting handlers so the lock isn't held for long
		waitingHandlersCopy = waitingHandlers;
		lck.unlock();


		for (auto it = waitingHandlersCopy.begin();
			it != waitingHandlersCopy.end();)
		{
			TorrentHandler* handler = *it;
			it = waitingHandlersCopy.erase(it);
			queuedHandlers.push_back(handler);
		}

		lck.lock();
		for (auto handler : queuedHandlers)
		{
			waitingHandlers.erase(handler);
			activeHandlers.insert(handler);

			_pool.push_task(callHandlerMessenger, handler, 15);
		}
		lck.unlock();

	}
}

/// <summary>
/// Creates a thread that will download the given torrent
/// </summary>
/// <param name="torrent">The torrent to download</param>
/// <param name="path">The path to download the files to</param>
const TorrentHandler* Client::addTorrent(const std::string& torrentPath, const std::string& dstPath)
{
	//Wait until a port is bound
	std::unique_lock lck(_startupMtx);
	_startupCv.wait(lck, [this] {return !_takenPort.empty(); });

	Torrent t = readTorrentFile(torrentPath);
	std::string infoHash = generateTorrentHash(torrentPath);
	if (_activeTorrents.count(infoHash))
	{
		std::cout << "Torrent already added\n";
		return nullptr;
	}

	std::string dst = dstPath == "" ? t.info->name : dstPath;
	auto handler = std::make_unique<TorrentHandler>(t, dst, infoHash);
	//TorrentHandler* handler = new TorrentHandler(t, dst, infoHash);


	{
		std::scoped_lock torrLck(_torrentsMtx);
		_activeTorrents[infoHash] = std::move(handler);
	}

	_torrentsCv.notify_all();

	return _activeTorrents[infoHash].get();
}

const TorrentHandler* Client::addTorrent(const std::string& torrentPath)
{
	return addTorrent(torrentPath, "");
}

/// <summary>
/// Sets everything necessary to start listening and then starts listening
/// </summary>
/// <param name="port">The port to try listening at</param>
void Client::listen(std::string port)
{
	int iResult = 0;

	struct addrinfo* result = NULL;
	struct addrinfo hints;

	int recvbuflen = DEFAULT_BUFLEN;


	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = AI_PASSIVE;

	// Resolve the server address and port
	iResult = getaddrinfo(NULL, port.c_str(), &hints, &result);
	if (iResult != 0) //If the initial port is taken keep trying until max port
	{
		printf("getaddrinfo failed with error: %d\n", iResult);
		throw std::exception();
	}

	// Create a SOCKET for connecting to server
	_listenSocket = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
	if (_listenSocket == INVALID_SOCKET)
	{
		printf("socket failed with error: %ld\n", WSAGetLastError());
		freeaddrinfo(result);
		throw std::exception();
	}

	// Setup the TCP listening socket
	iResult = bind(_listenSocket, result->ai_addr, (int)result->ai_addrlen);
	if (iResult == SOCKET_ERROR)
	{
		printf("bind failed with error: %d\n", WSAGetLastError());
		freeaddrinfo(result);
		closesocket(_listenSocket);
		throw std::exception();
	}

	freeaddrinfo(result);

	iResult = ::listen(_listenSocket, SOMAXCONN);
	if (iResult == SOCKET_ERROR)
	{
		printf("listen failed with error: %d\n", WSAGetLastError());
		closesocket(_listenSocket);
		throw std::exception();
	}

}

/// <summary>
/// Handshakes with peer and adds it to the correct handler
/// </summary>
/// <param name="peerSock">The peer socket</param>
void Client::addPeer(SOCKET peerSock)
{
	HandshakeMessage msg = Communication::receiveHandshake(peerSock);
	std::string infoHash = msg.infoHash;

	std::shared_lock lck(_torrentsMtx);
	if (!_activeTorrents.count(infoHash))
	{
		std::cout << std::format(
			"A client with sock {} was rejected due to unknown info hash \n",
			peerSock);

		closesocket(peerSock);
		return;
	}
	auto& handler = _activeTorrents[msg.infoHash];
	lck.unlock();

	Communication::sendHandshake(peerSock, infoHash, _id);
	handler->addPeer(peerSock, msg.peerId);
}

/// <summary>
/// Accepts incoming clients if possible and adds them to the 
/// list of the thread that currently downloads the same torrent
/// </summary>
void Client::acceptClients()
{
	int recvbuflen = DEFAULT_BUFLEN;

	SOCKET clientSocket = INVALID_SOCKET;

	while (true)
	{
		if (TorrentHandler::isPeerListFull())
		{
			//TODO: Wait until peer list isn't full
		}

		// Accept a client socket
		clientSocket = accept(_listenSocket, NULL, NULL);
		if (clientSocket == INVALID_SOCKET)
		{
			printf("accept failed with error: %d\n", WSAGetLastError());
			closesocket(_listenSocket);
			throw std::exception();
		}

		_pool.push_task([=]() {addPeer(clientSocket); });
	}
}


