#include <stdio.h>
#include "Client.h"
#include <chrono>
#include <thread>
#include <fstream>
#include <bitset>
#include <sys/stat.h>
#include "bencode.hpp"
#include <filesystem>
#include <unordered_set>

int main(int argc, const char* argv[])
{

	if (argc == 2)
	{
		if (!std::filesystem::exists(argv[1]))
		{
			std::cout << "File doesn't exist";
			return 1;
		}

		//128KB
		int l = ipow(2, 17);
		createTorrentFile(argv[1], ".", l, "127.0.0.1:1234/announce");
		return 0;
	}

	Client client;

	std::thread t(&Client::startConnection, std::ref(client));
	t.detach();


	//client.addTorrent("Harry Potter Script.pdf.torrent");
	//client.addTorrent("bittr folder example.torrent");
	//client.addTorrent("Panorama_of_3mb.jpg.torrent");

	
	while (true)
	{
		std::string str;
		std::getline(std::cin, str);

		try
		{
			client.addTorrent(str);
		}
		catch (...)
		{
			std::cout << "Error while adding torrent\n";
		}

	}
}