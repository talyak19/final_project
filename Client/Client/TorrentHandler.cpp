#include "TorrentHandler.h"
#include <ranges>
#include <memory>
#include "Communication.h"
#include <boost/thread/lockable_concepts.hpp>
int TorrentHandler::_PeerCount = 0;

TorrentHandler::TorrentHandler(const Torrent& t, const std::string& path, const std::string& infoHash) :
	_targetTorrent(t), _infoHash(infoHash)
{
	if (t.mode == InfoMode::Single)
	{
		_fileManager = new SingleFileManager(path, 
			(SingleModeInfo*)t.info.get());
	}
	else
	{
		_fileManager = new MultipleFileManager(path, 
			(MultiModeInfo*)t.info.get());
	}
	_isSeeding = _fileManager->finishedDownloading();
	
	int piecesDownloaded = 0;
	for (bool bit : _fileManager->getBitfield())
	{
		piecesDownloaded += bit;
	}
	_bytesDownloaded = piecesDownloaded * t.info->pieceLength;
}


TorrentHandler::~TorrentHandler()
{
}

const Torrent& TorrentHandler::getTorrent() const noexcept
{
	return _targetTorrent;
}

/// <summary>
/// Adds a peer to the peer list and sends a bitfield message
/// </summary>
/// <param name="peer">The peer to add</param>
void TorrentHandler::addPeer(SOCKET incomingPeer, std::string_view id)
{
	
	std::unique_lock lck(_idsMutex);
	if (isPeerInList(id, false))
	{
		//throw PeerAlreadyKnown(incomingPeer, id);
		std::cout << std::format(
			"Peer {} with id {} is already in list\n",
			incomingPeer, id);
		return;
	}
	_knownIds.emplace_back(id);
	lck.unlock();

	auto peer = std::make_unique<Peer>(incomingPeer, id,
		_targetTorrent.info->getPiecesCount());

	std::scoped_lock guard(_peerQueueMtx);
	_peerQueue.push_back(std::move(peer));
	_peerQueueCv.notify_all();
}

bool TorrentHandler::isPeerListFull() noexcept
{
	return _PeerCount > _MaxPeers;
}

/// <summary>
/// Uses shared mutex to check if peer with given id is already in list
/// </summary>
/// <param name="peerId">The id of a peer</param>
/// <returns>If the peer is already in peer list</returns>
bool TorrentHandler::isPeerInList(std::string_view peerId) const noexcept
{
	return isPeerInList(peerId, true);
}

/// <summary>
/// Checks if peer is already in peer list
/// </summary>
/// <param name="peerId">The id of a peer</param>
/// <param name="lockMtx">Whether to shared lock mutex or not</param>
/// <returns>If the peer is already in peer list</returns>
bool TorrentHandler::isPeerInList(std::string_view peerId, bool lockMtx) const noexcept
{
	auto check = [&]() {
		return std::ranges::count(_knownIds, peerId);
	};

	if (lockMtx)
	{
		std::shared_lock lck(_idsMutex);
		return check();
	}
	else
	{
		return check();
	}
}

/// <summary>
/// Inserts all peers in the peer queue into the messaging list and
/// clears the queue
/// </summary>
/// <returns></returns>
void TorrentHandler::insertIncomingPeers() noexcept
{
	std::unique_lock lck(_peerQueueMtx, std::defer_lock);

	if (!lck.try_lock())
	{
		return;
	}
	if (_peerQueue.empty())
	{
		return;
	}

	for (auto& peer : _peerQueue)
	{
		std::cout << std::format("Peer {} with id {} added to list\n",
			peer->getSocket(), peer->getId());
		_peers.push_back(std::move(peer));
	}

	_peerQueue.clear();
}

/// <summary>
/// Calculates the amount of blocks needed for each piece
/// Returns a different value for last piece
/// </summary>
/// <param name="pieceIndex">The index of the piece to check</param>
/// <returns>Amount of blocks needed for each piece</returns>
int TorrentHandler::getBlocksAmountInPiece(int pieceIndex) const noexcept
{
	int blocksPerPiece = 0;
	
	if (pieceIndex == 	_targetTorrent.info->getLastPieceIndex())
	{
		//Round if needed after division
		blocksPerPiece =
			_targetTorrent.info->getLastPieceLength() / _PieceBlockSize +
			(_targetTorrent.info->getLastPieceLength() % _PieceBlockSize != 0);
	}
	else
	{
		blocksPerPiece = _targetTorrent.info->pieceLength / _PieceBlockSize;
	}

	return blocksPerPiece;
}

/// <summary>
/// Requests a block of piece that wasn't already requested
/// </summary>
/// <param name="peer">The target peer</param>
/// <param name="pieceIndex">Index of the piece to downloaded a block of</param>
void TorrentHandler::requestPieceBlock(Peer* peer, int pieceIndex)
{
	int blocksPerPiece = getBlocksAmountInPiece(pieceIndex);

	//Using maps to keep track of requested and downloaded blocks
	if (_blocksRequested[pieceIndex] == nullptr)
	{
		_blocksRequested[pieceIndex] = createStaticBitset(blocksPerPiece);
		_blocksDownloaded[pieceIndex] = createStaticBitset(blocksPerPiece);
	}

	//Search for a block of the piece that wasn't requested
	for (int i = 0; i < blocksPerPiece; i++)
	{
		if (_blocksRequested[pieceIndex][i] == 0)
		{
			int blockSize = _PieceBlockSize;
			//Change block size if it's the last block
			if (pieceIndex == _targetTorrent.info->getLastPieceIndex())
			{
				if (i == blocksPerPiece - 1)
				{
					blockSize = _targetTorrent.info->getLastPieceLength() -
						(blocksPerPiece - 1) * _PieceBlockSize;
				}
			}
			peer->sendRequest(pieceIndex, i * _PieceBlockSize, blockSize);
			_blocksRequested[pieceIndex][i] = 1;

			return;
		}
	}
}

/// <summary>
/// Saves downloaded block in the file
/// </summary>
/// <param name="pieceMessage">The piece message containing the block</param>
void TorrentHandler::saveBlockInFile(int index, int begin, const std::vector<unsigned char>& block)
{

	int piecePosition = index * _targetTorrent.info->pieceLength;
	int position = piecePosition + begin;
	_fileManager->write(position, block);

	int blockIndex = begin / _PieceBlockSize;
	_blocksDownloaded[index][blockIndex] = 1;

	bool pieceFullyDownloaded = true;
	for (int i = 0; i < getBlocksAmountInPiece(index); i++)
	{
		if (_blocksDownloaded[index][i] == 0)
		{
			pieceFullyDownloaded = false;
		}
	}
	_bytesDownloaded += block.size();

	if (pieceFullyDownloaded)
	{
		_fileManager->updateBitfield(index);



		//Add all peers who don't have the piece to a list
		for (auto& peer : _peers)
		{
			if (peer->getBitfield()[index] == 0)
			{
				peer.get()->addPieceToInform(index);
			}
		}

		if (_fileManager->finishedDownloading())
		{
			_isSeeding = true;
			std::cout << "Finished downloading. Seeding now\n";

			_bytesDownloaded = _targetTorrent.info->getPiecesCount() * _targetTorrent.info->pieceLength;
		}

		for (auto& func : onPieceDownloadList)
		{
			func();
		}
	}
}


/// <summary>
/// Sends a block that is requested by a piece request
/// </summary>
/// <param name="peer">The requesting peer</param>
/// <param name="req">The received request</param>
/// <returns>Amount of bytes sent to peer</returns>
int TorrentHandler::respondToPieceRequest(Peer& peer, const RequestMessage& req)
{
	int position = req.index *
		_targetTorrent.info->pieceLength + req.begin;

	PieceMessage msg
	{
		req.index,
		req.begin,
		_fileManager->read(position, req.length)
	};

	return peer.sendMessage(msg);
}

/// <summary>
/// Looks for a piece that is both needed and is owned by the given peer
/// </summary>
/// <param name="peer">The target peer</param>
/// <returns>The index of the piece</returns>
int TorrentHandler::findOptimalPiece(Peer* peer)
{
	int pieceIndex = 0;
	for (int i = 0; i < _fileManager->getBitfield().size(); i++)
	{
		//Check that you don't have the piece and the peer has it
		if (_fileManager->getBitfield()[i] == 0 &&
			peer->getBitfield()[i] == 1)
		{
			pieceIndex = i;
			break;
		}
	}
	return pieceIndex;
}


/// <summary>
/// Removes peers who have a faulty socket and adds all peers that
/// can be communicated with to resVec
/// </summary>
/// <param name="res">Result of socket poll</param>
/// <param name="resVec">Vector to put communicable peers in</param>
void TorrentHandler::handlePollResult(const PollResults& res, std::vector<Peer*>& resVec)
{
	for (auto sock : res.eventFulfillingSockets)
	{
		auto it = std::ranges::find_if(_peers, [&](
			const std::unique_ptr<Peer>& p) {return p->getSocket() == sock; });
		if (it != _peers.end())
		{
			resVec.emplace_back((*it).get());
		}
	}

	//Remove all bad peers
	for (auto sock : res.badSockets)
	{
		std::erase_if(_peers, [&](const std::unique_ptr<Peer>& p) {
			return p.get()->getSocket() == sock; });
		std::cout << std::format(
			"Peer {} has been removed due to faulty connection\n", sock);
	}
}

/// <summary>
/// Receives all messages from peers and handles them
/// </summary>
void TorrentHandler::handleIncomingMessages()
{
	std::vector<SOCKET> peerSockets;
	for (auto& peer : _peers)
	{
		peerSockets.push_back(peer->getSocket());
	}

	auto pollRes = getReadableSockets(peerSockets);
	if (!pollRes)
	{
		return;
	}

	std::vector<Peer*> readablePeers;
	handlePollResult(*pollRes, readablePeers);
	if (readablePeers.empty())
	{
		return;
	}

	for (auto peer : readablePeers)
	{
		std::vector<unsigned char> buffer;
		MessageType type{};

		try
		{
			std::tie(type, buffer) = peer->receiveMessage();
		}
		catch (std::exception& e)
		{
			std::cout << e.what();
		}

		switch (type)
		{
		case Bitfield:
		{
			MessageDeserializer<BitfieldMessage> bitfieldDeserializer(buffer);
			peer->updateBitfieldMap(bitfieldDeserializer.message.bitfield);
			break;
		}

		case Have:
		{
			MessageDeserializer<HaveMessage> haveDeserializer(buffer);
			peer->updateBitfieldMap(haveDeserializer.message.index);
			break;
		}

		case Request:
		{
			MessageDeserializer<RequestMessage> requestDeserializer(buffer);
			peer->addReuqest(requestDeserializer.message);
			break;
		}

		case Piece:
		{
			MessageDeserializer<PieceMessage> pieceDeserializer(buffer);
			const PieceMessage& pieceMsg = pieceDeserializer.message;
			saveBlockInFile(pieceMsg.index, pieceMsg.begin, pieceMsg.block);
			peer->decreasePendingRequestsCount();
			break;
		}

		default:
			std::cout << "Unknown message\n";
			break;
		}

	}

}

/// <summary>
/// Sends requests to peers and responds to recieved requests
/// </summary>
void TorrentHandler::handleOutcomingMessages()
{
	std::vector<SOCKET> peerSockets;
	for (auto& peer : _peers)
	{
		peerSockets.push_back(peer->getSocket());
	}

	auto pollRes = getWriteableSockets(peerSockets);
	if (!pollRes)
	{
		return;
	}

	std::vector<Peer*> writeablePeers;
	handlePollResult(*pollRes, writeablePeers);
	if (writeablePeers.empty())
	{
		return;
	}

	for (auto peer : writeablePeers)
	{
		if (!peer->wasSentBitfield())
		{
			peer->sendBitfield(_fileManager->getBitfield());
			return;
		}

		auto requests = peer->getRequests();

		//Send blocks to all requesting peers
		if (!requests.empty())
		{
			for (auto& request : requests)
			{
				respondToPieceRequest(*peer, *request.get());
				std::cout << std::format("Sent block of length {} with"
					" offset {} of piece with index {} was sent to"
					" peer with id {}\n",  request->length, request->begin,
					request->index, peer->getId());
				peer->removeRequest(request);
			}
		}

		//Send have requests to all peers who need to know
		while (peer->getUninformedPiecesCount() > 0)
		{
			peer->sendHaveRequest();
		}

		if (!_isSeeding && peer->getPendingRequestCount() <= _MaxPendingRequests)
		{
			requestPieceBlock(peer, findOptimalPiece(peer));
		}
	}
}

/// <summary>
/// Proccesses received messages and sends messages to other peers
/// </summary>
void TorrentHandler::messagePeers()
{
	//Wait until a peer has connected if none are currently connected
	if (_peers.empty())
	{
		std::cout << std::format("No peers for file {}. Now waiting.",
			_targetTorrent.info->name);
		std::unique_lock lck(_peerQueueMtx);
		_peerQueueCv.wait(lck, [this] {return !_peerQueue.empty(); });
	}
	insertIncomingPeers();

	try
	{
		handleIncomingMessages();
		handleOutcomingMessages();
	}
	catch (PeerConnectionError& e)
	{
		std::cout << e.what();
	}
}

/// <summary>
/// Gets a function pointer to a function that will be called on piece download
/// </summary>
/// <param name="function">The function pointer</param>
void TorrentHandler::onPieceDownload(std::function<void()> function) const
{
	onPieceDownloadList.push_back(function);
}

float TorrentHandler::getPercentDownloaded() const
{
	int bytesInFile = _targetTorrent.info->getPiecesCount() * _targetTorrent.info->pieceLength;
	return ((float)_bytesDownloaded / bytesInFile) * 100;
}
