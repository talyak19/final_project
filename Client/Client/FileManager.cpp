#include "FileManager.h"
#include <cassert>

FileManager::FileManager(std::string_view path, const TorrentInfo* info)  
	: _targetPath(path), _rawInfo(info)
{
}


const std::vector<bool>& FileManager::getBitfield() const noexcept
{
	return _piecesBitfield;
}

void FileManager::updateBitfield(int pieceIndex)
{
	int length = pieceIndex == _rawInfo->getLastPieceIndex() ?
		_rawInfo->getLastPieceLength() : _rawInfo->pieceLength;

	auto res = read(_rawInfo->pieceLength * pieceIndex, length);

	bool valid = _rawInfo->comparePiece((char*)res.data(), length, pieceIndex);
	_piecesBitfield[pieceIndex] = valid;
	if (!valid)
	{
		std::cout << std::format("Piece {} is coruppted\n", pieceIndex);
	}
}

/// <returns>Returns true if every bit in the bitfield 
/// is set to 1</returns>
bool FileManager::finishedDownloading() const noexcept
{
	bool done = true;
	for (auto bit : _piecesBitfield)
	{
		if (bit == 0)
		{
			done = false;
		}
	}
	return done;
}
