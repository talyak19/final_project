#include "Peer.h"

Peer::Peer(SOCKET sock, std::string_view id, int bitfieldSize) :
	_socket(sock), _id(id), _bitfield(bitfieldSize), _pendingRequests(0), _wasSentBitfield(false)
{
}

Peer::~Peer()
{
	closesocket(_socket);
}


SOCKET Peer::getSocket() const noexcept
{
	return _socket;
}

std::string_view Peer::getId() const noexcept
{
	return _id;
}

const DynamicBitset& Peer::getBitfield() const noexcept
{
	return _bitfield;
}


/// <summary>
/// Reads from the socket a single message by reading the byte count and 
/// taking from the socket the content of the message
/// </summary>
/// <returns>A pair that includes the message type implied by
///  the id field and the message contents in a bytes vector</returns>
std::pair<MessageType, std::vector<unsigned char>> Peer::receiveMessage() const
{
	MessageType type{};
	int messageLen = 0;
	std::shared_ptr<char> lengthBuffer = Communication::recvFromSocket(_socket, _LengthFieldSize);
	std::memcpy(&messageLen, lengthBuffer.get(), sizeof(int));

	type = (MessageType) * (Communication::recvFromSocket(_socket, sizeof(MessageType)).get());

	std::shared_ptr<unsigned char> res = std::reinterpret_pointer_cast
		<unsigned char>(Communication::recvFromSocket(_socket, messageLen));

	std::vector<unsigned char> msgContents(res.get(), res.get() + messageLen);

	std::cout <<
		std::format
		(
			"{} Bytes received in Message of type {} from peer {}\n",
			messageLen + sizeof(MessageType) + sizeof(messageLen),
			(int)type,
			_socket
		);

	return std::pair<MessageType, std::vector<unsigned char>>(type, msgContents);
}

/// <summary>
/// Sends message in format <length prefix><message ID><payload>. The length 
/// prefix is a four byte big-endian value. The message ID is a single decimal 
/// byte.
/// </summary>
/// <param name="type">Number signifying the type of the message</param>
/// <param name="msg">The vector containing the bytes of the message</param>
/// <returns>Amount of bytes sent</returns>
int Peer::sendMessage(MessageType type, std::vector<unsigned char> msg) const
{
	std::vector<unsigned char> fullMsg;

	//Adding the <length> 
	fullMsg.resize(_LengthFieldSize);
	for (int i = 0; i < _LengthFieldSize; i++)
	{
		fullMsg[i] = (unsigned char)(msg.size() >> (i * _ByteSize));
	}

	//Adding the <ID> 
	fullMsg.emplace_back((unsigned char)type);

	//Adding the <payload> 
	fullMsg.insert
	(
		fullMsg.end(),
		std::make_move_iterator(msg.begin()),
		std::make_move_iterator(msg.end())
	);

	int res = 0;
	if ((res = send(_socket, (char*)fullMsg.data(), (int)fullMsg.size(), 0)) == SOCKET_ERROR)
	{
		printf("send failed with error: %d\n", WSAGetLastError());
		throw std::exception();
	}
	else
	{
		std::cout <<
			std::format("{} Bytes sent in Message of type {} to peer {}\n",
				res, (int)type, _socket);
	}

	return res;
}

int Peer::sendBitfield(DynamicBitset bitfield)
{
	BitfieldMessage BitfieldMsg{ convertFromBitfield(bitfield) };
	_wasSentBitfield = true;

	return sendMessage(BitfieldMsg);
}

/// <summary>
/// Sends a request message to this peer and increases the pending reqeusts count
/// </summary>
/// <param name="index">The index of the piece</param>
/// <param name="begin">Byte offset within the piece</param>
/// <param name="length">The requested length</param>
/// <returns>Amount of bytes sent</returns>
int Peer::sendRequest(int index, int begin, int length)
{
	RequestMessage requestMsg
	{
		index,
		begin,
		length
	};
	_pendingRequests++;
	std::cout << std::format("Requested block of length {} with offset {} of"
		" piece with index {} from peer with id {}\n", length, 
		begin, index, _id);
	return sendMessage(requestMsg);
}

/// <summary>
/// Sends a have message with index of piece that was added to inform queue
/// </summary>
/// <returns>Amount of bytes sent</returns>
int Peer::sendHaveRequest()
{
	if (_uninformedPiecesIndices.size() == 0)
	{
		throw std::exception("No pieces to inform about in queue");
	}
	HaveMessage msg{};
	msg.index = _uninformedPiecesIndices.front();
	_uninformedPiecesIndices.pop();

	return sendMessage(msg);
}


/// <summary>
/// Updates the pieces bitfield that belongs to the given peer
/// </summary>
/// <param name="bitfieldMsg">The bitfield message sent by the peer</param>
/// <returns></returns>
void Peer::updateBitfieldMap(const std::vector<unsigned char>& bitfield) noexcept
{
	_bitfield.clear();

	for (auto byte : bitfield)
	{
		//Reverse byte since we're iterating from the end
		byte = (byte & 0xF0) >> 4 | (byte & 0x0F) << 4;
		byte = (byte & 0xCC) >> 2 | (byte & 0x33) << 2;
		byte = (byte & 0xAA) >> 1 | (byte & 0x55) << 1;

		for (int i = 0; i < _ByteSize; i++)
		{
			_bitfield.push_back((byte >> i) % 2);
		}
	}
}

/// <summary>
/// Updates the pieces bitfield that belongs to the given peer
/// </summary>
/// <param name="index">The index of the piece that was downloaded</param>
/// <returns></returns>
void Peer::updateBitfieldMap(int index) noexcept
{
	_bitfield[index] = 1;
}

void Peer::addReuqest(RequestMessage& req)
{
	_requests.push_back(std::make_shared<RequestMessage>(req));
}

void Peer::removeRequest(const std::shared_ptr<RequestMessage> req)
{
	std::erase(_requests, req);
}

const std::vector<std::shared_ptr<RequestMessage>>& Peer::getRequests() const
{
	return _requests;
}

int Peer::getPendingRequestCount() const
{
	return _pendingRequests;
}

void Peer::decreasePendingRequestsCount()
{
	_pendingRequests--;
}

/// <summary>
/// Adds an index of piece to queue of uninformed pieces that will 
/// be sent in have message
/// </summary>
/// <param name="index">Index of the piece</param>
void Peer::addPieceToInform(int index)
{
	if (_bitfield[index] == 1)
	{
		throw std::exception("Piece is already downloaded");
	}
	_uninformedPiecesIndices.push(index);
}

int Peer::getUninformedPiecesCount() const
{
	return _uninformedPiecesIndices.size();
}

bool Peer::wasSentBitfield() const noexcept
{
	return _wasSentBitfield;
}

